﻿using System;
using System.Collections.Generic;
using EscuelaNet.Dominio.Proyectos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Capacitaciones.Test
{
    [TestClass]
    public class UnitTestCapacitaciones
    {
        [TestMethod]
        public void PROBAR_CAPACITACION()
        {
            var capa = new Capacitacion();
            Assert.AreEqual(0, capa.GetDuracion());
            Action action = () =>
            {
                capa.SetDuracion(-10);

            };
            Assert.ThrowsException<ExcepcionDeProyectos>(action);
        }

        [TestMethod]
        public void ADD_ALUMNO()
        {
            var capa = new Capacitacion();
            var edad = DateTime.Now;
            capa.PushAlumno("matias","juarez","40111222",edad);
            Assert.AreEqual("matias", capa.Alumnos[0].Nombre);

        }

        [TestMethod]
        public void ADD_CONOCIMIENTO()
        {
            var capa = new Capacitacion();

            capa.AddTema("MVC",1);

            Assert.AreEqual("MVC", capa.Temas[0].Nombre);

        }

        [TestMethod]
        public void ADD_INSTRUCTOR()
        {
            var capa = new Capacitacion();
            capa.AddTema("MVC",1);
            capa.AddTema("MVC5",1);

            var edad = DateTime.Now;

            var instructor = new Instructor()
            {
                Nombre = "Jhonny",
                Apellido = "Sang",
                Dni = "39888777",
                FechaNacimiento = edad,
                Temas= new List<Tema>()
            };
            var instructor2 = new Instructor()
            {
                Nombre = "Gaston",
                Apellido = "Fernandez",
                Dni = "39444555",
                FechaNacimiento = edad,
                Temas = new List<Tema>()
            };

            instructor.Temas.Add(new Tema("ASP.NET",1));
            instructor2.Temas.Add(new Tema("MVC",1));

            capa.AddInstructor(instructor);
            capa.AddInstructor(instructor2);
        }
    }
}
