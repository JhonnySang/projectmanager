﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{

        public class Programador : Entity
        {

            public string Nombre { get; set; }
            public NivelConocimiento NivelesConocimiento { get; set; }
            public Programador(string nombre)
            {
                this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
                this.NivelesConocimiento = NivelConocimiento.Junior;
            }
        }

    }

