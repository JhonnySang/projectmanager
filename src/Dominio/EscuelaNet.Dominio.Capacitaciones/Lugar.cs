﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Lugar : Entity
    {
        public string Descripcion { get; set; }
    }
}
