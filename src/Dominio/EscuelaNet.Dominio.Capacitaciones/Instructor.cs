﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Instructor : Persona
    {
        public IList<Tema> Temas { get; set; }

        public Instructor()
        {

        }

        public Instructor(string nombre, string apellido, string dni, DateTime fechaNacimiento) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Apellido = apellido ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Dni = dni ?? throw new System.ArgumentNullException(nameof(nombre));
            this.FechaNacimiento = fechaNacimiento;
            this.Temas = new List<Tema>();
        }

        public void AddTemaInstructor(string nombre,int nivel)
        {
            this.Temas.Add(new Tema(nombre,nivel));

        }
    }
}
