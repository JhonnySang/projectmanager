﻿using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Tema : Entity
    {
        public string Nombre { get; set; }
        public int Nivel { get; set; }
        private Tema()
        {

        }

        public Tema(string nombre, int nivel) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Nivel = nivel;
        }
    }
}