﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Capacitacion : Entity, IAggregateRoot
    {
        public IList<Instructor> Instructores { get; set; }
        public Lugar Lugar { get; set; }
        public int Minimo { get; set; }
        public int Maximo { get; set; }
        private int Duracion { get; set; }
        public decimal Precio { get; set; }
        public DateTime Inicio { get; set; }
        public IList<Alumno> Alumnos { get; set; }
        public IList<Tema> Temas { get; set; }

        public void SetDuracion(int duracion)
        {
            if (duracion <= 0)
                throw new ExcepcionDeProyectos("Error La Duracion de la Capacitacion no puede ser 0");
            Duracion = duracion;
        }
        public int GetDuracion()
        {
            return Duracion;
        }

        public void PushAlumno(string nombre,string apellido,string dni, DateTime fechaNacimiento)
        {
            if (this.Alumnos == null)
            {
                this.Alumnos = new List<Alumno>();
            }
            this.Alumnos.Add(new Alumno(nombre, apellido, dni, fechaNacimiento));
        }

        public void AddInstructor(Instructor instructor)
        {
            if (this.Instructores == null)
            {
                this.Instructores = new List<Instructor>();
            }
            foreach (var tema in this.Temas)
            {
                foreach (var temaInstructor in instructor.Temas)
                {
                    if (temaInstructor.Nombre == tema.Nombre)
                    {
                        this.Instructores.Add(instructor);
                    }
                }
            }
        }

        public void AddTema(string nombre,int nivel)
        {
            if (this.Temas == null)
            {
                this.Temas = new List<Tema>();
            }
            this.Temas.Add(new Tema(nombre,nivel));
        }
    }
}
