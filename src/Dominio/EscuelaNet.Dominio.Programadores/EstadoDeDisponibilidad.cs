﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public enum EstadoDeDisponibilidad
    {
        Disponible,
        PartTime,
        FullTime
    }
}
