﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public class Equipo : Entity
    {
        public string Nombre { get; set; }
        public string Pais { get; set; }
        public int HusoHorario { get; set; }
        public IList<Programador> Programadores { get; set; }
        public IList<Conocimiento> Skills { get; set; }

        public Equipo(string nombre,string pais,int hora)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Pais = pais ?? throw new System.ArgumentNullException(nameof(pais));
            this.HusoHorario = VerificarHusoHorario(hora);
        }
        
        private Equipo() {}


        public void PushProgramador(Programador programador)
        {
            if(this.Programadores == null)
            {
                this.Programadores = new List<Programador>();  
            }
            foreach (var conocimiento in this.Skills)
            {
                if (programador.Skills == null || programador.Skills.Contains(conocimiento))
                {
                    throw new Exception("No contiene los conocimientos del equipo");
                }
            }
            this.Programadores.Add(programador);
        }

        public void PushSkill(string nombre)
        {
            if (this.Skills == null)
            {
                this.Skills = new List<Conocimiento>();
            }

            this.Skills.Add(new Conocimiento(nombre));
        }

        public int VerificarHusoHorario(int husohorario)
        {
            if(husohorario >= -12 && husohorario <= 14)
            {
                return husohorario;
            }
            else
            {
                throw new ExcepcionDeEquipo("El HusoHorario no esta en el rango establecido");
            }
            
        }
        
    }
}