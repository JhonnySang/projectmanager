﻿using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Proyectos
{
    public class LineaDeProduccion : Entity
    {
        public string Nombre { get; set; }
        private LineaDeProduccion()
        {

        }
        public LineaDeProduccion(string Nombre)
        {
            this.Nombre = Nombre;
        }
    }
}