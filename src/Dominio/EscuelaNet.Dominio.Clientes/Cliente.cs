﻿using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;
using System.Linq;

namespace EscuelaNet.Dominio.Clientes
{
    public class Cliente : Entity, IAggregateRoot
   {
        public string RazonSocial { get; set; }

        public string Email { get; set; } 

        public Categoria Categoria { get; set; }

        public IList<UnidadDeNegocio> Unidades { get; private set; } 

        private Cliente() { }

        public Cliente(string razonSocial, string email, Categoria categoria)
        {
            this.RazonSocial = razonSocial ?? throw new System.ArgumentNullException(nameof(razonSocial));
            this.Email = email ?? throw new System.ArgumentNullException(nameof(email));
            this.Categoria = categoria;
        }

        public void AgregarUnidad(UnidadDeNegocio unidad)
        {
            if (this.Unidades == null)
            {
                this.Unidades = new List<UnidadDeNegocio>();
            }
            if(!this.Unidades.Any(x => x.RazonSocial == unidad.RazonSocial))
            {
                this.Unidades.Add(unidad);
            }
            
        }

        public double Humor()
        {
            var cantidadSolicitudesTotal = 0;
            var cantidadSolicitudesBDA = 0;
            if (this.Unidades == null)
            {
                return 0.0;
            }
            else
            {
                foreach (var unidad in this.Unidades)
                {
                    if (unidad.Solicitudes!=null)
                    {
                        foreach (var solicitud in unidad.Solicitudes)
                        {
                            cantidadSolicitudesTotal++;

                            if (   solicitud.Estado==EstadoSolicitud.Borrador 
                                || solicitud.Estado==EstadoSolicitud.Desarrollo 
                                || solicitud.Estado==EstadoSolicitud.Abandonado)
                            {
                                cantidadSolicitudesBDA++;
                            }
                        }
                    }
                }

                if (cantidadSolicitudesTotal == 0)
                {
                    return 0.0;
                }
                else
                {
                    var humor = 100 * (cantidadSolicitudesTotal - cantidadSolicitudesBDA) / cantidadSolicitudesTotal;
                    return humor;
                }

            }           
        }

    }

}