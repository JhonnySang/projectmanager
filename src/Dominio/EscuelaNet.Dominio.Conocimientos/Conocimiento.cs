﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{    public class Conocimiento : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public IList<Asesor> Asesores { get; set; }
        public Demanda Demanda { get; set; }
        public IList<ConocimientoPorPrecio> ConocimientoPorprecio { set; get; }
        public IList<Archivo> Archivos { get; set; }

        public Conocimiento(String nombre)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
        }

        public void AgregarConocimientoPorPrecio(Nivel nivel, Double valorNominal, string moneda )
        {
            if (ConocimientoPorprecio == null)
            {
                ConocimientoPorprecio = new List<ConocimientoPorPrecio>();
            }
            ConocimientoPorprecio.Add(new ConocimientoPorPrecio
            {
                ValorNominal = valorNominal,
                Moneda = moneda,
                Nivel = nivel,
                Fecha = DateTime.Now
            });
        }

        public void AgregarAsesor(string nombre, string apellido, string idioma, string pais)
        {
            if (Asesores == null)
            {
                Asesores = new List<Asesor>();
            }
            this.Asesores.Add(new Asesor
            {
                Nombre = nombre,
                Apellido = apellido,
                Idioma = idioma,
                Pais = pais
            });

        }


    }
}

