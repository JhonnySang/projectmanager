﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
  public  class Asesor
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public Disponibilidad Disponibilidad { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Idioma { get; set; }
        public String Pais { get; set; }

        public Asesor(string nombre, string apellido, string idioma, String pais)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre),nameof(apellido));
            this.Apellido = apellido ?? throw new System.ArgumentNullException(nameof(apellido));
            this.Idioma = idioma;
            this.Pais = Pais;
        }
        public Asesor()
        {

        }
        private Asesor(DateTime fechaNacimiento)
        {
            this.FechaNacimiento = fechaNacimiento;
        }
    }
}
