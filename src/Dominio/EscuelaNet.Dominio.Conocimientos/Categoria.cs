﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
   public class Categoria : Entity
    {
        public string Descripcion { get; set; }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public IList<Conocimiento> Conocimientos { get; set; }

        public Categoria(int id,string nombre, string descripcion)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Descripcion = descripcion;
            this.Id = id;
        }
        private Categoria()
        {
        }
    }
}
