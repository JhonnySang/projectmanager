﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Clientes.Test
{
    [TestClass]
    public class UnitTestClientes
    {
        [TestMethod]
        public void PROBAR_CREAR_UNA_DIRECCION()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            string direccionPrueba = "domicilio1, localidad1, provincia1, pais1";
            Assert.AreEqual(direccionPrueba, direccion.toString());
        }
        
        [TestMethod]
        public void PROBAR_CREAR_UNA_UNIDAD()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var unidad = new UnidadDeNegocio("razonSocial", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233", direccion);
            Assert.AreEqual("razonSocial", unidad.RazonSocial);
            Assert.AreEqual("responsable1", unidad.ResponsableDeUnidad);
            Assert.AreEqual("20-1122334455-7", unidad.Cuit);
            Assert.AreEqual("responsable@email.com", unidad.EmailResponsable);
            Assert.AreEqual("03814112233", unidad.TelefonoResponsable);
            Assert.IsTrue(unidad.Direcciones.Contains(direccion));
        }

        [TestMethod]
        public void PROBAR_AGREGAR_DIRECCION()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var unidad = new UnidadDeNegocio("razonSocial", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233", direccion);
            
            var direccion2 = new Direccion("domicilio2", "localidad2", "provincia2", "pais2");
            unidad.AgregarDireccion(direccion2);

            string direccionPrueba2 = "domicilio2, localidad2, provincia2, pais2";
            Assert.AreEqual(direccionPrueba2, unidad.Direcciones[1].toString());
        }

        [TestMethod]
        public void PROBAR_AGREGAR_MISMA_DIRECCION()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var direccion2 = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");

            var unidad = new UnidadDeNegocio("razonSocial", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233", direccion);
           
            unidad.AgregarDireccion(direccion2);

            Assert.AreEqual(1, unidad.Direcciones.Count);
        }

        [TestMethod]
        public void PROBAR_CREAR_UN_CLIENTE()
        {
            var cliente = new Cliente("nombre", "email@email.com", Categoria.Gobierno);

            Assert.AreEqual("nombre", cliente.RazonSocial);
            Assert.AreEqual("email@email.com", cliente.Email);
            Assert.AreEqual(Categoria.Gobierno, cliente.Categoria);
        }

        [TestMethod]
        public void PROBAR_AGREGAR_UNIDAD()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var unidad = new UnidadDeNegocio("razonSocial", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233", direccion);
            var cliente = new Cliente("nombre", "email@email.com", Categoria.Gobierno);

            cliente.AgregarUnidad(unidad);

            Assert.IsTrue(cliente.Unidades.Contains(unidad));
        }

        [TestMethod]
        public void PROBAR_CREAR_SOLICITUD()
        {
            var solicitud = new Solicitud("titulo", "descripcion");

            Assert.AreEqual("titulo", solicitud.Titulo);
            Assert.AreEqual("descripcion", solicitud.Descripcion);
            Assert.AreEqual(EstadoSolicitud.Borrador, solicitud.Estado);
        }

        [TestMethod]
        public void PROBAR_AGREGAR_SOLICITUD()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var unidad = new UnidadDeNegocio("razonSocial", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233", direccion);
            var solicitud = new Solicitud("titulo", "descripcion");

            unidad.AgregarSolicitud(solicitud);

            Assert.IsTrue(unidad.Solicitudes.Contains(solicitud));
        }

        [TestMethod]
        public void PROBAR_CAMBIAR_ESTADO_SOLICITUD()
        {
            var solicitud = new Solicitud("titulo", "descripcion");

            //de Borrador a Desarrollo
            solicitud.CambiarEstado(EstadoSolicitud.Desarrollo);
            Assert.AreEqual(EstadoSolicitud.Desarrollo, solicitud.Estado);

            //de Desarrollo a Produccion
            solicitud.CambiarEstado(EstadoSolicitud.Produccion);
            Assert.AreEqual(EstadoSolicitud.Produccion, solicitud.Estado);

            //de Produccion a Deprecado
            solicitud.CambiarEstado(EstadoSolicitud.Deprecado);
            Assert.AreEqual(EstadoSolicitud.Deprecado, solicitud.Estado);

            //de Deprecado a Desarrollo NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(()=> solicitud.CambiarEstado(EstadoSolicitud.Desarrollo));
           
            //de Deprecado a Produccion NO DEBE CAMBIAR            
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud.CambiarEstado(EstadoSolicitud.Produccion));

            //de Deprecado a Borrador NO DEBE CAMBIAR            
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud.CambiarEstado(EstadoSolicitud.Borrador));

            //de Deprecado a Abandonado NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud.CambiarEstado(EstadoSolicitud.Abandonado));

            var solicitud2 = new Solicitud("titulo2", "descripcion2");
            solicitud2.CambiarEstado(EstadoSolicitud.Desarrollo);

            //de Desarrollo a Abandonado
            solicitud2.CambiarEstado(EstadoSolicitud.Abandonado);
            Assert.AreEqual(EstadoSolicitud.Abandonado, solicitud2.Estado);

            //de Abandonado a Desarrollo NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Desarrollo));
            
            //de Abandonado a Produccion NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Produccion));

            //de Abandonado a Deprecado NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Deprecado));

            //de Abandonado a Borrador
            solicitud2.CambiarEstado(EstadoSolicitud.Borrador);
            Assert.AreEqual(EstadoSolicitud.Borrador, solicitud2.Estado);

            //de Borrador a Abandonado NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Abandonado));

            //de Borrador a Produccion NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Produccion));

            //de Borrador a Deprecado NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Deprecado));

            solicitud2.CambiarEstado(EstadoSolicitud.Desarrollo);
            //de Desarrollo a Borrador NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Borrador));

            //de Desarrollo a Deprecado NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Deprecado));

            solicitud2.CambiarEstado(EstadoSolicitud.Produccion);
            //de Produccion a Borrador NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Borrador));

            //de Produccion a Abandonado NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Abandonado));

            //de Produccion a Desarrollo NO DEBE CAMBIAR
            Assert.ThrowsException<ExcepcionDeCliente>(() => solicitud2.CambiarEstado(EstadoSolicitud.Desarrollo));

        }

        [TestMethod]
        public void PROBAR_HUMOR_CLIENTE()
        {
            var solicitud1 = new Solicitud("titulo1", "descripcion1");
            var solicitud2 = new Solicitud("titulo2", "descripcion2");
            var solicitud3 = new Solicitud("titulo3", "descripcion3");
            var solicitud4 = new Solicitud("titulo4", "descripcion4");

            var direccion1 = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var unidad1 = new UnidadDeNegocio("razonSocial1", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233", direccion1);

            unidad1.AgregarSolicitud(solicitud1);
            solicitud2.CambiarEstado(EstadoSolicitud.Desarrollo);
            solicitud2.CambiarEstado(EstadoSolicitud.Produccion);
            unidad1.AgregarSolicitud(solicitud2);

            var direccion2 = new Direccion("domicilio2", "localidad2", "provincia2", "pais2");
            var unidad2 = new UnidadDeNegocio("razonSocial2", "responsable2", "20-1122334455-7", "responsable@email.com", "03814112233", direccion2);

            unidad2.AgregarSolicitud(solicitud3);
            unidad2.AgregarSolicitud(solicitud4);

            solicitud3.CambiarEstado(EstadoSolicitud.Desarrollo);
            solicitud3.CambiarEstado(EstadoSolicitud.Produccion);

            solicitud4.CambiarEstado(EstadoSolicitud.Desarrollo);
            solicitud4.CambiarEstado(EstadoSolicitud.Produccion);
            solicitud4.CambiarEstado(EstadoSolicitud.Deprecado);


            var cliente = new Cliente("nombre", "email@email.com", Categoria.Gobierno);

            cliente.AgregarUnidad(unidad1);
            cliente.AgregarUnidad(unidad2);

            var humor = cliente.Humor();

            Assert.AreEqual(75.0, humor);
        }

    }
}
