﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Programadores.Test
{
    [TestClass]
    public class UnitTestProgramadores
    {
        [TestMethod]
        public void PROBAR_CREAR_UN_PROGRAMADOR()
        {
            var programador = new Programador("Cristian", "Martinez", 42201, "43501357", "Programador", DateTime.Now);
            Assert.AreEqual(EstadoDeDisponibilidad.PartTime.ToString(), programador.ConsultarDisponibilidad());

            programador.PushConocimiento("Java");
            programador.PushConocimiento("Python");

            Action accion = () =>
            {
                var equipo = new Equipo("Alfa", "Italia", -13);
                equipo.PushSkill(".Net");
                equipo.PushProgramador(programador);
            };
            Assert.ThrowsException<ExcepcionDeEquipo>(accion);

        }

        [TestMethod]
        public void PROBAR_CONSULTA_DISPONIBILIDAD()
        {
            var programador = new Programador("Cristian", "Martinez", 42201, "43501357", "Programador", DateTime.Now);
            programador.CambiarDisponibilidad(EstadoDeDisponibilidad.FullTime);
            Assert.AreEqual(EstadoDeDisponibilidad.FullTime.ToString(), programador.ConsultarDisponibilidad());
            programador.CambiarDisponibilidad(EstadoDeDisponibilidad.PartTime);
            Assert.AreEqual(EstadoDeDisponibilidad.FullTime.ToString(), programador.ConsultarDisponibilidad());
        }
    }
}
