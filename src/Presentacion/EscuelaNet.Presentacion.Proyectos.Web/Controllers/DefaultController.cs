﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Presentacion.Proyectos.Web.Infraestructura;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class LineasController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            var lineasDeProduccion = Contexto.Instancia.LineasDeProduccion;
            var model = new LineasIndexModel()
            {
                Titulo = "Primera pruba",
                LineasDeProduccion = lineasDeProduccion
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevaLineaModel();
            return View(model);

        }
        [HttpPost]
        public ActionResult New(NuevaLineaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.LineasDeProduccion.Add(new LineaDeProduccion(model.Nombre));
                    TempData["success"] = "Linea de producción creada";
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
               
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        public ActionResult Edit(int id)
        {
            var linea = Contexto.Instancia.LineasDeProduccion[id];
            var model = new NuevaLineaModel() {
                Nombre = linea.Nombre,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(NuevaLineaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.LineasDeProduccion[model.Id].Nombre 
                        = model.Nombre;
                    TempData["success"] = "Linea de producción editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }
        public ActionResult Delete(int id)
        {
            var linea = Contexto.Instancia.LineasDeProduccion[id];
            var model = new NuevaLineaModel()
            {
                Nombre = linea.Nombre,
                Id = id
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Delete(NuevaLineaModel model)
        {
           
                try
                {
                    Contexto.Instancia.LineasDeProduccion.RemoveAt(model.Id);
                    TempData["success"] = "Linea de producción borrada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

           

        }


    }
}