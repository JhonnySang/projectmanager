﻿using EscuelaNet.Presentacion.Clientes.Web.Infraestructura;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class ClientesController : Controller
    {
        // GET: Clientes
        public ActionResult Index()
        {
            var clientes = Contexto.Instancia.Clientes;

            var model = new ClientesIndexModel()
            {
                Titulo = "Primera prueba",
                Clientes = clientes
            };

            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevoClienteModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevoClienteModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    Contexto.Instancia.Clientes.Add(new Dominio.Clientes.Cliente(
                            model.RazonSocial, model.Email, model.Categoria
                        ));
                    TempData["success"] = "Cliente creado";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var cantidad = Contexto.Instancia.Clientes.Count();
            if (id < 0 || id >= cantidad)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else { 
            var cliente = Contexto.Instancia.Clientes[id];

            var model = new NuevoClienteModel()
            {
                Id = id,
                RazonSocial = cliente.RazonSocial,
                Email = cliente.Email,
                Categoria = cliente.Categoria
            };

            return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevoClienteModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    Contexto.Instancia.Clientes[model.Id].RazonSocial = model.RazonSocial;
                    Contexto.Instancia.Clientes[model.Id].Email = model.Email;
                    Contexto.Instancia.Clientes[model.Id].Categoria = model.Categoria;

                    TempData["success"] = "Cliente editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            var cantidad = Contexto.Instancia.Clientes.Count();
            if (id < 0 || id >= cantidad)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else { 
                var cliente = Contexto.Instancia.Clientes[id];
            
                var model = new NuevoClienteModel()
                {
                    Id = id,
                    RazonSocial = cliente.RazonSocial,
                    Email = cliente.Email,
                    Categoria = cliente.Categoria
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevoClienteModel model)
        {

            try
            {
                Contexto.Instancia.Clientes.RemoveAt(model.Id);
                TempData["success"] = "Cliente borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
            
        }

    }
}