﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();

        private Contexto()
        {
            this.Clientes = new List<Cliente>();

            Clientes.Add(new Cliente("Arcor", "arcor@gmail.com", Categoria.Privado));

            Clientes.Add(new Cliente("Pepsi", "pepsi@gmail.com", Categoria.Privado));

            Clientes.Add(new Cliente("Samsung", "samsung@gmail.com", Categoria.Privado));

        }

        public List<Cliente> Clientes { get; set; }

        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }
        }
    }
}