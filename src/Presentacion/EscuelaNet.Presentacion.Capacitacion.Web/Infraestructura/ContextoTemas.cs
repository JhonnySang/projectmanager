﻿using System;
using EscuelaNet.Dominio.Capacitaciones; 
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura
{
    public sealed class ContextoTemas
    {
        private static ContextoTemas _instancia = new ContextoTemas();
        public List<Tema> Temas { get; set; }


        private ContextoTemas()
        {

            this.Temas = new List<Tema>();

            this.Temas.Add(new Tema("MVC", 1));
            this.Temas.Add(new Tema(".Net Core", 2));
            this.Temas.Add(new Tema("Java", 3));

        }

        public static ContextoTemas Instancia
        {
            get
            {
                return _instancia;
            }
        }
    }
}