﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura
{
    public sealed class ContextoInstructor
    {
        private static ContextoInstructor _instancia = new ContextoInstructor();

        public List<Instructor> Instructores { get; set; }


        private ContextoInstructor()
        {
            this.Instructores = new List<Instructor>();
            this.Instructores.Add(new Instructor("Matias","Apellido","1234567",DateTime.Today));
            this.Instructores.Add(new Instructor("Juancito", "Pepo", "123123", DateTime.Today));
            this.Instructores.Add(new Instructor("Ramoncito", "Segundo", "1234567", DateTime.Today));

        }

        public static ContextoInstructor Instancia
        {
            get
            {
                return _instancia;
            }
        }
    }
}