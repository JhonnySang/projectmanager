﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class TemaController : Controller
    {
        // GET: Tema
        public ActionResult Index()
        {
            var temas = ContextoTemas.Instancia.Temas;
            var model = new TemaIndexModel()
            {
                Titulo = "Primera prueba",
                Temas = temas
            };
            return View(model);
        }

        // GET: Tema/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Tema/New
        public ActionResult New()
        {
            var model = new NuevoTemaModel();
            return View(model);
        }

        // POST: Tema/New
        [HttpPost]
        public ActionResult New(NuevoTemaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                if (model.Nivel <= 0)
                {
                    TempData["error"] = "Nivel Debe Ser Mayor a 0";
                    return View(model);
                }
                else
                {
                    try
                    {
                        ContextoTemas.Instancia.Temas.Add(new Tema(model.Nombre, model.Nivel));

                        TempData["success"] = "Tema creado";
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        TempData["error"] = ex.Message;
                        return View(model);
                    }


                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }
        }

        // GET: Tema/Edit/5
        public ActionResult Edit(int id)
        {
            var tema = ContextoTemas.Instancia.Temas[id];
            var model = new NuevoTemaModel()
            {
                Nombre = tema.Nombre,
                Id = id,
                Nivel = tema.Nivel
            };
            return View(model);
        }

        // POST: Tema2/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoTemaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                if (model.Nivel <= 0)
                {
                    TempData["error"] = "Nivel Debe Ser Mayor a 0";
                    return View(model);
                }
                else
                {
                    try
                    {
                        ContextoTemas.Instancia.Temas[model.Id].Nombre
                            = model.Nombre;
                        ContextoTemas.Instancia.Temas[model.Id].Nivel
                            = model.Nivel;

                        TempData["success"] = "Tema Editado";
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        TempData["error"] = ex.Message;
                        return View(model);
                    }

                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        // GET: Tema/Delete/5
        public ActionResult Delete(int id)
        {
            var tema = ContextoTemas.Instancia.Temas[id];
            var model = new NuevoTemaModel()
            {
                Nombre = tema.Nombre,
                Id = id,
                Nivel = tema.Nivel,
            };
            return View(model);
        }

        // POST: Tema/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoTemaModel model)
        {
            try
            {
                ContextoTemas.Instancia.Temas.RemoveAt(model.Id);
                TempData["success"] = "Tema borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}