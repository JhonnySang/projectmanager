﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class InstructorController : Controller
    {
        // GET: Instructor
        public ActionResult Index()
        {
            var instructores = ContextoInstructor.Instancia.Instructores;
            var model = new InstructorIndexModel()
            {
                Titulo = "Primera prueba",
                Instructores = instructores,

            };
            return View(model);
        }


        // GET: Instructor/New
        public ActionResult New()
        {
            var model = new NuevoInstructorModel();
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult New(NuevoInstructorModel model)
        {
            if (string.IsNullOrEmpty(model.Nombre) || string.IsNullOrEmpty(model.Apellido)
                                                   || string.IsNullOrEmpty(model.Dni))
            {

                TempData["error"] = "Nada de Espacios en Blanco";
                return View(model);
            }
            else
            {
                try
                {
                    ContextoInstructor.Instancia.Instructores.Add(new Instructor(model.Nombre, model.Apellido,
                        model.Dni, model.FechaNacimiento));

                    TempData["success"] = "Instructor Agregado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }

        }

        // GET: Instructor/Edit/5
        public ActionResult Edit(int id)
        {
            var instructor = ContextoInstructor.Instancia.Instructores[id];
            var model = new NuevoInstructorModel()
            {
                Nombre = instructor.Nombre,
                Id = id,
                Apellido = instructor.Apellido,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
            };
            return View(model);
        }

        // POST: Instructor/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoInstructorModel model)
        {
            if (string.IsNullOrEmpty(model.Nombre) || string.IsNullOrEmpty(model.Apellido)
                                                   || string.IsNullOrEmpty(model.Dni))
            {

                TempData["error"] = "Nada de espacios en Blanco";
                return View(model);
            }
            else
            {
                try
                {
                    ContextoInstructor.Instancia.Instructores[model.Id].Nombre
                        = model.Nombre;
                    ContextoInstructor.Instancia.Instructores[model.Id].Apellido
                        = model.Apellido;
                    ContextoInstructor.Instancia.Instructores[model.Id].Dni
                        = model.Dni;
                    ContextoInstructor.Instancia.Instructores[model.Id].FechaNacimiento
                        = model.FechaNacimiento;


                    TempData["success"] = "Instructor Editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
        }
    


        // GET: Instructor/Delete/5
        public ActionResult Delete(int id)
        {
            var instructor = ContextoInstructor.Instancia.Instructores[id];
            var model = new NuevoInstructorModel()
            {
                Nombre = instructor.Nombre,
                Id = id,
                Apellido = instructor.Apellido,
                Dni = instructor.Dni,
                FechaNacimiento = instructor.FechaNacimiento,
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoTemaModel model)
        {
            try
            {
                ContextoInstructor.Instancia.Instructores.RemoveAt(model.Id);
                TempData["success"] = "Instructor borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}
