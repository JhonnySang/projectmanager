﻿namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class NuevoTemaModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Nivel { get; set; }
    }
}